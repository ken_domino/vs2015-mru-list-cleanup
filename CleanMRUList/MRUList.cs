﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Win32;
using static System.Environment;

namespace CleanMRUList
{
    public class MRUList : ObservableCollection<MRUItem>
    {
        public static MRUList Singleton;

        public MRUList()
        {
            //RegistryKey localMachine64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
            //RegistryKey regKey = localMachine64.OpenSubKey(@"HKEY_CURRENT_USER", false);
            // Open registry and look for MRU list of Visual Studio.
            RegistryKey current_user = Registry.CurrentUser;
            RegistryKey pRegKey = current_user.OpenSubKey(@"SOFTWARE\Microsoft\VisualStudio\14.0\MRUItems\{a9c4a31f-f9cb-47a9-abc0-49ce82d0b3ac}\Items");
            foreach (string str in pRegKey.GetValueNames())
            {
                String v = (String)pRegKey.GetValue(str);
                String n = "-";
                String l = "-";
                String dc = "-";
                String da = "-";
                String a = "no";
                Match match = Regex.Match(v, @"([^|]+)[|]([^|]+)[|]([^|]+)[|]([^|]+)[|]");
                if (match.Success)
                {
                    // Finally, we get the Group value and display it.
                    l = match.Groups[1].Value;
                    l = ExpandEnvironmentVariables(l);
                    n = match.Groups[4].Value;
                    n = ExpandEnvironmentVariables(n);
                }
                // Go to file system and check...
                FileInfo info = new FileInfo(l);
                if (info.Exists)
                {
                    a = "yes";
                    FileAttributes attributes = info.Attributes;
                    dc = File.GetCreationTime(l).ToString();
                    da = File.GetLastAccessTime(l).ToString();
                }
                MRUItem i = new MRUItem()
                {
                    Name = n,
                    Location = l,
                    Available = a,
                    DateAccessed = da,
                    DateCreated = dc,
                    FullKey = v
                };
                Add(i);
            }
            Singleton = this;
        }

        public void RemoveKey(MRUItem item)
        {
            RegistryKey current_user = Registry.CurrentUser;
            RegistryKey pRegKey = current_user.OpenSubKey(@"SOFTWARE\Microsoft\VisualStudio\14.0\MRUItems\{a9c4a31f-f9cb-47a9-abc0-49ce82d0b3ac}\Items", true);
            foreach (string str in pRegKey.GetValueNames())
            {
                String v = (String)pRegKey.GetValue(str);
                if (v.Equals(item.FullKey))
                {
                    pRegKey.DeleteValue(str);
                    Remove(item);
                    return;
                }
            }
        }

        public void Reorder(String key)
        {
            // Make a copy of the list and re-enter the contents sorted by the key.
            IOrderedEnumerable<MRUItem> sorted = null;
            if (key.Equals("Location"))
                sorted = this.OrderBy(x => x.Location);
            else if (key.Equals("Name"))
                sorted = this.OrderBy(x => x.Name);
            else if (key.Equals("Available"))
                sorted = this.OrderBy(x => x.Available);
            else if (key.Equals("DateCreated"))
                sorted = this.OrderBy(x => x.DateCreated);
            else if (key.Equals("DateAccessed"))
                sorted = this.OrderBy(x => x.DateAccessed);
            else if (key.Equals("FullKey"))
                sorted = this.OrderBy(x => x.FullKey);
            else throw new Exception("Cannot sort!");
            List<MRUItem> li = sorted.ToList();

            RegistryKey current_user = Registry.CurrentUser;
            RegistryKey pRegKey = current_user.OpenSubKey(@"SOFTWARE\Microsoft\VisualStudio\14.0\MRUItems\{a9c4a31f-f9cb-47a9-abc0-49ce82d0b3ac}\Items", true);
            int i = 0;
            foreach (string str in pRegKey.GetValueNames())
            {
                String v = (String)pRegKey.GetValue(str);
                String new_v = li[i++].FullKey;
                pRegKey.SetValue(str, new_v);
            }
        }
    }
}
