﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanMRUList
{
    public class MRUItem
    {
        public String Name { get; set; }
        public String Location { get; set; }
        public String Available { get; set; }
        public String DateCreated { get; set; }
        public String DateAccessed { get; set; }
        public String FullKey { get; set; }
    }
}
