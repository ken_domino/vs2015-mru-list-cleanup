﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CleanMRUList
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ColumnHeaderClick(object sender, RoutedEventArgs e)
        {
            String col = ((DataGridColumnHeader)e.Source).Content.ToString();
            MRUList.Singleton.Reorder(col);
        }

        private void DoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGridRow dgr = (DataGridRow) sender;
            MRUItem mru_item = dgr.Item as MRUItem;
            // Open this item.
            System.Diagnostics.Process.Start(mru_item.Location);
        }

        private void MyCommand(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                DataGrid dg = (DataGrid) sender;
                // Make g.d. copy of selected items.
                List<MRUItem> list = new List<MRUItem>();
                foreach (MRUItem i in dg.SelectedItems)
                    list.Add(i);
                foreach (MRUItem i in list)
                {
                    MRUList.Singleton.RemoveKey(i);
                }
                e.Handled = true;
            }
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }
    }
}
